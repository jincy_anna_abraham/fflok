package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

public class LoginSuccess {

    private String recipient;

    @SerializedName("country_code")
    private String countryCode;

    private float cost;

    @SerializedName("sms_needed")
    private int smsNeeded;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public int getSmsNeeded() {
        return smsNeeded;
    }

    public void setSmsNeeded(int smsNeeded) {
        this.smsNeeded = smsNeeded;
    }
}
