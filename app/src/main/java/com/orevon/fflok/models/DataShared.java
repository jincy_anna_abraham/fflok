package com.orevon.fflok.models;

public class DataShared {
    String name;
    int color;
    boolean dataShared;

    public DataShared(String name, int color, boolean dataShared) {
        this.name = name;
        this.color = color;
        this.dataShared = dataShared;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isDataShared() {
        return dataShared;
    }

    public void setDataShared(boolean dataShared) {
        this.dataShared = dataShared;
    }
}
