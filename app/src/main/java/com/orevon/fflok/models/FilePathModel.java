package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

public class FilePathModel {
    @SerializedName("_id")
    private String id;
    private String path;
    private Boolean visible;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }


}
