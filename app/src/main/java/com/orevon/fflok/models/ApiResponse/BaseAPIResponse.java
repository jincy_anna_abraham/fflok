package com.orevon.fflok.models.ApiResponse;

public class BaseAPIResponse {
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
