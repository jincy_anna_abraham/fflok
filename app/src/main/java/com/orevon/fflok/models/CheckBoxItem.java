package com.orevon.fflok.models;

public class CheckBoxItem {
    String title;
    String subTitle;
    boolean isChecked = false;

    public CheckBoxItem(String title, String subTitle, boolean isChecked) {
        this.title = title;
        this.subTitle = subTitle;

        this.isChecked = isChecked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
