package com.orevon.fflok.models;

import java.util.List;

public class ProfileListItem {
    String title;
    String subTitle;
    int rating;
    int ratingTotal;
    int color;
    int imageId;
    int progressDrawableImageId;
    String ratingDescription;
    boolean isExpanded = false;
    List<CheckBoxItem> checkBoxItems;

    public ProfileListItem(String title, String subTitle, int rating, int ratingTotal, String ratingDescription, int color, int imageId, int progressDrawableImageId) {
        this.title = title;
        this.subTitle = subTitle;
        this.rating = rating;
        this.ratingTotal = ratingTotal;
        this.ratingDescription = ratingDescription;
        this.color = color;
        this.imageId = imageId;
        this.progressDrawableImageId = progressDrawableImageId;

    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRatingDescription() {
        return ratingDescription;
    }

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getRatingTotal() {
        return ratingTotal;
    }

    public void setRatingTotal(int ratingTotal) {
        this.ratingTotal = ratingTotal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public List<CheckBoxItem> getCheckBoxItems() {
        return checkBoxItems;
    }

    public void setCheckBoxItems(List<CheckBoxItem> checkBoxItems) {
        this.checkBoxItems = checkBoxItems;
    }

    public int getProgressDrawableImageId() {
        return progressDrawableImageId;
    }

    public void setProgressDrawableImageId(int progressDrawableImageId) {
        this.progressDrawableImageId = progressDrawableImageId;
    }
}
