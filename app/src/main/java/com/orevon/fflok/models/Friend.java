package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

public class Friend {
    private Boolean like;
    private Integer score;
    private String date;
    @SerializedName("_id")
    private String id1;
    private User id;

    public Boolean getLike() {
        return like;
    }

    public void setLike(Boolean like) {
        this.like = like;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }


    public User getId() {
        return id;
    }

    public void setId(User id) {
        this.id = id;
    }
}
