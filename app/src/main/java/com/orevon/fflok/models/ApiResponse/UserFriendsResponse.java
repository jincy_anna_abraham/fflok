package com.orevon.fflok.models.ApiResponse;

import com.google.gson.annotations.SerializedName;
import com.orevon.fflok.models.Friend;

import java.util.List;

public class UserFriendsResponse {
    private Boolean success;

    @SerializedName("number likers")
    private int numberOfFriends;

    @SerializedName("likres ")
    private List<Friend> likers;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public int getNumberOfFriends() {
        return numberOfFriends;
    }

    public void setNumberOfFriends(int numberOfFriends) {
        this.numberOfFriends = numberOfFriends;
    }


    public List<Friend> getLikers() {
        return likers;
    }

    public void setLikers(List<Friend> likers) {
        this.likers = likers;
    }
}
