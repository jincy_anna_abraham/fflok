package com.orevon.fflok.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BaselineLayout;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.orevon.fflok.R;
import com.orevon.fflok.fragments.EventDialogFragment;
import com.orevon.fflok.fragments.EventMapFragment;
import com.orevon.fflok.fragments.EventSingleDialogFragment;
import com.orevon.fflok.fragments.ExplorerFragment;
import com.orevon.fflok.fragments.HomeFragment;
import com.orevon.fflok.fragments.MatchFragment;
import com.orevon.fflok.fragments.MyFflokFragment;
import com.orevon.fflok.fragments.ProbabilityFragment;
import com.orevon.fflok.fragments.ProfileFragment;
import com.orevon.fflok.handlers.ShakeEventHandler;
import com.orevon.fflok.listeners.ShakeEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ShakeEventHandler,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private ShakeEventListener mShakeListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        setContentView(R.layout.activity_main);
        initControls();
        initShakeListener();
        loadHomeFragment();
    }

    @Override
    public void handleShakeEvent() {
//        handler.postDelayed(changeColorRunnable, 0);
//        makePhoneIconVibrate();
//        handler.postDelayed(changeColorRunnable, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mShakeListener != null) {
            mShakeListener.registerHandler(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mShakeListener != null) {
            mShakeListener.unregisterHandler(this);
        }
    }

    private void initShakeListener() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            mShakeListener = new ShakeEventListener(sensorManager, accelerometer);
        }
    }

    private void initControls() {
        initBottomNavigationMenu();
    }

    private void initBottomNavigationMenu() {
        BottomNavigationView mainBottomNavigation = findViewById(R.id.mainBottomNavigation);
        mainBottomNavigation.setSelectedItemId(R.id.bottomBarBtn3);
        removeShiftMode(mainBottomNavigation);
        mainBottomNavigation.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_bottom));
        mainBottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    private void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);

            Typeface tfRegular = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Book.otf");
            Typeface tfBold = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Bold.otf");

            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());

                View itemTitle = item.getChildAt(1);

                ((TextView) ((BaselineLayout) itemTitle).getChildAt(0))
                        .setTypeface(i == 2 ? tfBold : tfRegular);
                ((TextView) ((BaselineLayout) itemTitle).getChildAt(1))
                        .setTypeface(i == 2 ? tfBold : tfRegular);
            }
        } catch (NoSuchFieldException e) {
            Log.e("BottomNav", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BottomNav", "Unable to change value of shift mode", e);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.bottomBarBtn1:
                fragment = new ProfileFragment();
                break;
            case R.id.bottomBarBtn2:
                fragment = new MyFflokFragment();
                break;
            case R.id.bottomBarBtn3:
                fragment = new HomeFragment();
                break;
            case R.id.bottomBarBtn4:
                fragment = new MatchFragment();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        EventDialogFragment fr = EventDialogFragment.newInstance();
//                        fr.show(getFragmentManager(), "");
//
//                        EventSingleDialogFragment fr2 = EventSingleDialogFragment.newInstance();
//                        fr2.show(getFragmentManager(), "");
////
//                        ProbabilityFragment fr3 = ProbabilityFragment.newInstance();
//                        fr3.show(getFragmentManager(), "");
//                    }
//                }, 500);
                break;
            case R.id.bottomBarBtn5:
                fragment = new ExplorerFragment();
                break;
        }
        if (fragment != null) {
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_frame, fragment).commit();
        }
        return true;
    }

    private void loadHomeFragment() {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_frame, new HomeFragment()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventMapScreen event) {
        Fragment fragment = new EventMapFragment();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_frame, fragment).commit();
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    public static class EventMapScreen {

        public EventMapScreen() {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }


}
