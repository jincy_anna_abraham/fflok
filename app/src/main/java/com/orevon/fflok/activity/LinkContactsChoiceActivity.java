package com.orevon.fflok.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.orevon.fflok.R;

public class LinkContactsChoiceActivity extends AppCompatActivity {

    private final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    private ImageView flyingBird;

    private float xCoOrdinate, yCoOrdinate;
    private float flyingBirdOriginalX, flyingBirdOriginalY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_contacts_choice);
        animateHeader();
        initControls();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_CONTACTS
                    },
                    PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            Intent intent = new Intent(LinkContactsChoiceActivity.this, InviteContactsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    Intent intent = new Intent(LinkContactsChoiceActivity.this, InviteContactsActivity.class);
                    startActivity(intent);
                } else {
                    // Permission was not granted.
                    // Stay in this activity
                    flyingBird.setVisibility(View.VISIBLE);
                    flyingBird.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_top_left));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            flyingBirdOriginalX = flyingBird.getX();
                            flyingBirdOriginalY = flyingBird.getY();
                        }
                    }, 600);
                }
            }
        }
    }

    private void initControls() {
        initFlyingBird();

        Button buttonContinueWithoutLinkingContacts = findViewById(R.id.buttonContinueWithoutLinkingContacts);
        buttonContinueWithoutLinkingContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(LinkContactsChoiceActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initFlyingBird() {
        flyingBird = findViewById(R.id.top2AX);
        addDragEventToCoolBirdsContainer();
    }

    private void animateHeader() {
        View header = findViewById(R.id.header);
        header.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_top));
    }

    private void addDragEventToCoolBirdsContainer() {
        flyingBird.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        xCoOrdinate = view.getX() - event.getRawX();
                        yCoOrdinate = view.getY() - event.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        view.animate().x(event.getRawX() + xCoOrdinate).y(event.getRawY() + yCoOrdinate).setDuration(0).start();
                        break;
                    case MotionEvent.ACTION_UP:
                        view.animate().x(flyingBirdOriginalX).y(flyingBirdOriginalY).setDuration(500).start();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }
}
