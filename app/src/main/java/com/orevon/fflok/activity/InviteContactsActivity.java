package com.orevon.fflok.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.orevon.fflok.R;
import com.orevon.fflok.adapters.ContactRecordAdapter;
import com.orevon.fflok.models.ContactRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class InviteContactsActivity extends AppCompatActivity {

    static final int INITIALS_MAX_LEN = 2;
    ArrayList<ContactRecord> contactRecords;
    ListView listViewContacts;
    Cursor phones;
    ContentResolver resolver;
    ContactRecordAdapter contactRecordsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_contacts);

        animateHeaderAndFooter();

        View buttonInviteFromSocialMedia = findViewById(R.id.buttonInviteFromSocialMedia);
        buttonInviteFromSocialMedia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(InviteContactsActivity.this, InviteSocialMediaContactsActivity.class);
                startActivity(intent);
            }
        });

        View buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(InviteContactsActivity.this, ViewFriendsSpotsActivity.class);
                startActivity(intent);
            }
        });
        contactRecords = new ArrayList<ContactRecord>();
        resolver = this.getContentResolver();
        listViewContacts = findViewById(R.id.listViewContacts);

        phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        ContactLoaderTask contactLoader = new ContactLoaderTask();
        contactLoader.execute();

    }

    @Override
    protected void onStop() {
        super.onStop();
        phones.close();
    }

    private void animateHeaderAndFooter() {
        View header = findViewById(R.id.header);
        View footer = findViewById(R.id.footer);

        header.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_top));
        footer.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_bottom));
    }

    // Load data on background
    class ContactLoaderTask extends AsyncTask<Void, Void, Void> {
        Comparator<ContactRecord> wrapMe = new Comparator<ContactRecord>() {
            public int compare(ContactRecord o1, ContactRecord o2) {
                if (o1.getThumbnail() == null && o2.getThumbnail() == null) {
                    return 0;
                } else if (o1.getThumbnail() == null && o2.getThumbnail() != null) {
                    return 1;
                } else if (o1.getThumbnail() != null && o2.getThumbnail() == null) {
                    return -1;
                }
                return 0;
            }
        };

        private String getInitials(String contactId, String displayName) {
            // TODO: Fix
            // For now get the initials from the displayName
            // Querying for given-name and family-name is taking awefully long
            /*
            try {
                String structuredNameWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                String[] structuredNameWhereParams = new String[]{
                        contactId,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
                };
                Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, structuredNameWhere, structuredNameWhereParams, null);
                if (cursor.moveToFirst()) {
                    String givenName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                    String familyName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                    StringBuilder initialsBuilder = new StringBuilder();
                    if (!givenName.isEmpty()) {
                        initialsBuilder.append(givenName.charAt(0));
                    }
                    if (!familyName.isEmpty()) {
                        initialsBuilder.append(familyName.charAt(0));
                    }
                    return initialsBuilder.toString().toUpperCase();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            */

            String[] parts = displayName.split(" ");
            StringBuilder initialsBuilder = new StringBuilder();
            for (String part : parts) {
                initialsBuilder.append(part.charAt(0));
                if (initialsBuilder.length() == INITIALS_MAX_LEN) {
                    break;
                }
            }

            return initialsBuilder.toString().toUpperCase();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            Looper.prepare();
            if (phones != null) {
                Random rand = new Random();
                int phonesCount = phones.getCount();
                int fflockerCount = 8;
                int superFflockerCount = 4;
                ArrayList<ContactRecord> contactRecordsTemp = new ArrayList<>();

                while (phones.moveToNext()) {
                    --phonesCount;
                    Bitmap bitmapThumbnail = null;
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String uriThumbnail = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                    try {
                        if (uriThumbnail != null) {
                            bitmapThumbnail = MediaStore.Images.Media.getBitmap(resolver, Uri.parse(uriThumbnail));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String initials = null;
                    if (bitmapThumbnail == null) {
                        String contactId = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                        if ((contactId != null) && (!contactId.isEmpty())) {
                            initials = getInitials(contactId, name);
                            /* To convert initials to a bitmap
                            TODO: Revisit

                            if ((initials != null) && (!initials.isEmpty())) {
                                bitmapThumbnail = Bitmap.createBitmap(64, 64, Bitmap.Config.ARGB_8888);
                                Canvas c = new Canvas(bitmapThumbnail);
                                Paint paint = new Paint();
                                int xPos = (c.getWidth() / 2);
                                int yPos = (c.getHeight() / 2);
                                Rect bounds = new Rect();
                                paint.getTextBounds(initials, 0, initials.length(), bounds);
                                paint.setTextAlign(Paint.Align.CENTER);
                                c.drawText(initials, xPos, yPos + bounds.height() * 0.5f, paint);
                            }
                            */
                        }
                    }

                    ContactRecord contactRecord = new ContactRecord();
                    contactRecord.setThumb(bitmapThumbnail);
                    contactRecord.setName(name);
                    contactRecord.setPhone(phoneNumber);
                    contactRecord.setInitials(initials);

//                    // TODO: Replace with actual logic
//                    // For demo, Randomly pick some super fflockers and flockers for now
//                    if (fflockerCount > 0) {
//                        int n = rand.nextInt(1000);
//                        if ((phonesCount < fflockerCount) || ((n > 900) && (n % 13 == 0))) {
//                            --fflockerCount;
//                            contactRecord.setFflocker(true);
//                        }
//                    }
//                    if (superFflockerCount > 0) {
//                        int n = rand.nextInt(1000);
//                        if ((phonesCount < superFflockerCount) || ((n > 900) && (n % 13 == 0))) {
//                            --superFflockerCount;
//                            contactRecord.setSuperFflocker(true);
//                            contactRecord.setFflocker(true);
//                        }
//                    }

                    contactRecordsTemp.add(contactRecord);

                }

                // To sort the list in the order
                //      SuperFflockrs
                //      Flockers
                //      Non-members
                for (ContactRecord contactRecord : contactRecordsTemp) {
                    if (contactRecord.isSuperFflocker()) {
                        contactRecords.add(contactRecord);
                    }
                }
                for (ContactRecord contactRecord : contactRecordsTemp) {
                    if (!contactRecord.isSuperFflocker() && contactRecord.isFflockerFriend()) {
                        contactRecords.add(contactRecord);
                    }
                }
                for (ContactRecord contactRecord : contactRecordsTemp) {
                    if (!contactRecord.isFflockerFriend()) {
                        contactRecords.add(contactRecord);
                    }
                }
            }
            try {
                Collections.sort(contactRecords, wrapMe);
            } catch (Exception e) {
                Log.e("Sort error", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            contactRecordsAdapter = new ContactRecordAdapter(contactRecords, InviteContactsActivity.this);
            listViewContacts.setAdapter(contactRecordsAdapter);

            listViewContacts.setFastScrollEnabled(true);
        }
    }
}
