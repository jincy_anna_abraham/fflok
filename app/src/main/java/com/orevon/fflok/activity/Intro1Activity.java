package com.orevon.fflok.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.orevon.fflok.R;

public class Intro1Activity extends AppCompatActivity {

    RelativeLayout coolBirdsContainer;
    private ImageView rightAX;
    private ImageView rightBX;
    private ImageView rightCX;
    private ImageView rightDX;
    private ImageView leftAX;
    private ImageView leftBX;
    private ImageView leftCX;
    private ImageView leftDX;
    private ImageView topAX;
    private float xCoOrdinate, yCoOrdinate;
    private float birdsLayoutOriginalX, birdsLayoutOriginalY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_introduction);
        initControls();
    }

    private void initControls() {
        initCoolBirds();
        Button buttonPass = findViewById(R.id.buttonPass1);
        buttonPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToIntro2();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                leftDX.setImageResource(R.drawable.bird_eye_blink_animation);
                final AnimationDrawable frameAnimation = (AnimationDrawable) leftDX.getDrawable();
                frameAnimation.start();
            }
        }, 2500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                leftCX.setImageResource(R.drawable.bird_head_move_animation);
                final AnimationDrawable frameAnimation1 = (AnimationDrawable) leftCX.getDrawable();
                frameAnimation1.start();
            }
        }, 4500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                makeCoolBirdsFly();
            }
        }, 0);

        findViewById(R.id.pageIndicator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToIntro2();
            }
        });
    }

    private void goToIntro2() {
        finish();
        Intent intent = new Intent(Intro1Activity.this, Intro2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    private void initCoolBirds() {
        coolBirdsContainer = findViewById(R.id.coolBirdsLayout);

        final ConstraintLayout layout = findViewById(R.id.constraintLayout);

        layout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        birdsLayoutOriginalX = coolBirdsContainer.getX();
                        birdsLayoutOriginalY = coolBirdsContainer.getY();

                        layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

        addDragEventToCoolBirdsContainer();

        rightAX = findViewById(R.id.rightAX);
        rightBX = findViewById(R.id.rightBX);
        rightCX = findViewById(R.id.rightCX);
        rightDX = findViewById(R.id.rightDX);

        leftAX = findViewById(R.id.leftAX);
        leftBX = findViewById(R.id.leftBX);
        leftCX = findViewById(R.id.leftCX);
        leftDX = findViewById(R.id.leftDX);

        topAX = findViewById(R.id.topAX);
    }

    private void makeCoolBirdsFly() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rightAX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_top_right));
                rightBX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right));
                rightCX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right));
                rightDX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right));

                leftAX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_top_left));
                leftBX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));
                leftCX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));
                leftDX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));

                topAX.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_top));
            }
        }, 100);
    }


    private void addDragEventToCoolBirdsContainer() {
        coolBirdsContainer.findViewById(R.id.coolBirdsLayout).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        xCoOrdinate = view.getX() - event.getRawX();
                        yCoOrdinate = view.getY() - event.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        view.animate().x(event.getRawX() + xCoOrdinate).y(event.getRawY() + yCoOrdinate).setDuration(0).start();
                        break;
                    case MotionEvent.ACTION_UP:
                        view.animate().x(birdsLayoutOriginalX).y(birdsLayoutOriginalY).setDuration(500).start();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }
}
