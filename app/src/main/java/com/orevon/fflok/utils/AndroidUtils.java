package com.orevon.fflok.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import com.orevon.fflok.R;

import java.util.ArrayList;

/**
 * Created at Magora Systems (http://magora-systems.com) on 14.07.16
 *
 * @author Stanislav S. Borzenko
 */
public class AndroidUtils {
    @ColorInt
    public static int getColor(Context context, @ColorRes int colorResId) {
        int color;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            color = context.getColor(colorResId);
        } else {
            //noinspection deprecation
            color = context.getResources().getColor(colorResId);
        }

        return color;
    }

    @ColorInt
    public static int getThemeColor(Context context, @AttrRes int resourceId) {
        TypedValue typedValue = new TypedValue();
        TypedArray typedArray = context.obtainStyledAttributes(typedValue.data,
                new int[]{resourceId});
        try {
            return typedArray.getColor(0, 0);
        } finally {
            typedArray.recycle();
        }
    }

    public static Drawable getDrawable(Context context, int resourceId) {
        Drawable iconDrawable;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            iconDrawable = context.getResources()
                    .getDrawable(resourceId, context.getTheme());
        } else {
            //noinspection deprecation
            iconDrawable = context.getResources().getDrawable(resourceId);
        }

        return iconDrawable;
    }

    public static Drawable getTintDrawableByColor(
            Context context,
            @DrawableRes int drawableResId,
            @ColorInt int color) {
        Drawable drawable = AndroidUtils.getDrawable(context, drawableResId);
//        if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
//            drawable = drawable.mutate();
//        }

        Drawable drawableCompat = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawableCompat, color);

        return drawableCompat;
    }

    public static Drawable getTintDrawableByColor2(
            Context context,
            @DrawableRes int drawableResId,
            @ColorInt int color) {
        Drawable drawable = AndroidUtils.getDrawable(context, drawableResId);

//        if (DrawableUtils.canSafelyMutateDrawable(drawable)) {
//            drawable = drawable.mutate();
//        }

        PorterDuffColorFilter filter
                = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        drawable.setColorFilter(filter);

        return drawable;
    }

    public static Drawable getTintDrawableByColorRes(
            Context context,
            @DrawableRes int drawableResId,
            @ColorRes int colorResId) {
        int color = AndroidUtils.getColor(context, colorResId);
        return getTintDrawableByColor(context, drawableResId, color);
    }

    public static Drawable getTintDrawableByThemeAttr(
            Context context,
            @DrawableRes int drawableResId,
            @AttrRes int attrResId) {
        int color = AndroidUtils.getThemeColor(context, attrResId);
        return getTintDrawableByColor(context, drawableResId, color);
    }

    public static Drawable getTintDrawableByThemeAttr2(
            Context context,
            @DrawableRes int drawableResId,
            @AttrRes int attrResId) {
        int color = AndroidUtils.getThemeColor(context, attrResId);
        return getTintDrawableByColor2(context, drawableResId, color);
    }

    @DrawableRes
    public static int getSelectableItemBackground(Context context) {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int backgroundResource = typedArray.getResourceId(0, 0);
        typedArray.recycle();

        return backgroundResource;
    }

    public static int getStatusBarHeight(Context context) {
        int resource = context.getResources()
                .getIdentifier("status_bar_height", "dimen", "android");
        if (resource > 0) {
            return context.getResources().getDimensionPixelSize(resource);
        }

        return -1;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static float pxToDp(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static int dpToPx(final Context context, final int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static void setMarginEnd(View view, int marginEnd) {
        ViewGroup.MarginLayoutParams layoutParams =
                (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        MarginLayoutParamsCompat.setMarginEnd(layoutParams, marginEnd);
        view.setLayoutParams(layoutParams);
    }

    public static void setViewWidth(View view, int width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = width;
        view.setLayoutParams(layoutParams);
    }

    public static void setOverflowButtonColor(final Activity activity, final int color) {
        final String overflowDescription = activity.getString(R.string.abc_action_menu_overflow_description);
        final ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();
        final ViewTreeObserver viewTreeObserver = decorView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final ArrayList<View> outViews = new ArrayList<View>();
                decorView.findViewsWithText(outViews, overflowDescription, View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
                if (outViews.isEmpty()) {
                    return;
                }
                AppCompatImageView overflow = (AppCompatImageView) outViews.get(0);
                overflow.setColorFilter(color);
//                removeOnGlobalLayoutListener(decorView, this);
            }
        });
    }

//    public static void setTransParentToolbar(ActionBar actionBar, Activity activity) {
//        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(
//                activity, R.color.yourTranslucentColor)));
//    }

    public static boolean setActionBarBack(MenuItem item, Activity activity) {
        switch (item.getItemId()) {
            case android.R.id.home:
                activity.onBackPressed();
                return true;
            default:
                return activity.onOptionsItemSelected(item);
        }
    }

//    public static void setActionBarTitle(int stringId, AppCompatActivity activity, String font) {
//        setActionBarTitle(activity.getString(stringId), activity, font);
//    }

//    public static void setActionBarTitle(String text, AppCompatActivity activity, String font) {
//        SpannableString s = new SpannableString(text);
//        s.setSpan(new TypefaceSpan(activity, font), 0, s.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        activity.getSupportActionBar().setTitle(s);
//    }

    public static void setDialogDimension(Activity activity, Dialog dialog) {
        Context context = activity.getApplicationContext();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenHeight = displayMetrics.heightPixels;
        int screenWidth = displayMetrics.widthPixels;
        int dialogWidth = screenWidth - AndroidUtils.dpToPx(context, 32); // dialog width - padding 16dp
        int dialogHeight = (int) (.74 * screenHeight); // dialog height 74% of screen height
        dialog.getWindow().setLayout(dialogWidth, dialogHeight);
    }

    public static void showNoInternetSnackBar(final Activity activity) {
        Snackbar.make(activity.findViewById(android.R.id.content), "No internet connection.", Snackbar.LENGTH_LONG)
                .setAction("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.startActivityForResult(
                                new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    }
                })
                .show();
    }

    public static void initAndSetText(View view, int resId, String text) {
        TextView textview = view.findViewById(resId);
        textview.setText(text);
    }

    public static void setTransparentStatusBar(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setNavigationBarColor(Color.BLACK);
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }


    public static void applyFontForToolbarTitle(Context context, Toolbar toolbar) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                Typeface titleFont = Typeface.
                        createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
                if (tv.getText().equals(toolbar.getTitle())) {
                    tv.setTypeface(titleFont);
                    break;
                }
            }
        }
    }

    public static void setStatusBarColor(Window window, Context context, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = window.getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(context, color));
        }
    }

    public static void customizeDialog(Dialog dialog) {
        dialog.getWindow().getAttributes().windowAnimations = R.style.MessageDialogTheme;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }
}
