package com.orevon.fflok.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.location.Location;

import com.orevon.fflok.models.Fflok;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.models.User;

import java.util.List;

import static com.orevon.fflok.constants.FflokConstants.API_TOKEN;
import static com.orevon.fflok.constants.FflokConstants.FFLOK_SHARED_PREFS;
import static com.orevon.fflok.constants.FflokConstants.HOME_PAGE_FIRST_TIME_LOADED;
import static com.orevon.fflok.constants.FflokConstants.USER_ID;

public class FflokDataSingleton {
    private static final FflokDataSingleton ourInstance = new FflokDataSingleton();
    //TODO:remove this token
//    private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6Iis5MTk5OTU0MDA1OTYiLCJpYXQiOjE1MzA1MjM5NTl9.mIpu-AqQkIY0K8V3iEELbdhH-r7C3r40jScPUx4p00E";
   //Hari's token
//    private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6Iis5MTk5NjE5MzkwNzciLCJpYXQiOjE1MzA5NTkwODN9.watG3oNXrA9ids-JGqwqTBJsQsziTEVsDpTkPBXLRRo";

    private String token = "";
    private User user;
    private String userId = "";
    private Fflok fflok;
    private List<Place> proximatePlaces;
    private String fflokId;
    private Double latitude,longitude;
    private Boolean isNewUser;
    private Boolean isHomePageLoadedFirstTime;

    public static FflokDataSingleton getInstance() {
        return ourInstance;
    }

    private FflokDataSingleton() {
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getNewUser() {
        return isNewUser;
    }

    public void setNewUser(Boolean newUser) {
        isNewUser = newUser;
    }

    public String getToken(Context context) {
        if(token == "") {
            SharedPreferences sharedPreferences = context.getSharedPreferences(FFLOK_SHARED_PREFS, Context.MODE_PRIVATE);
            token = sharedPreferences.getString(API_TOKEN, "");
        }
        return token;
    }

    public void setToken(String token, Context context) {
        this.token = token;
        SharedPreferences sharedPreferences = context.getSharedPreferences(FFLOK_SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(API_TOKEN, token);
        editor.commit();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserId(Context context) {
        if(userId == "") {
            SharedPreferences sharedPreferences = context.getSharedPreferences(FFLOK_SHARED_PREFS, Context.MODE_PRIVATE);
            userId = sharedPreferences.getString(USER_ID, "");
        }
        return userId;
    }

    public void setUserId(String userId, Context context) {
        this.userId = userId;
        SharedPreferences sharedPreferences = context.getSharedPreferences(FFLOK_SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, userId);
        editor.commit();
    }

    public Boolean getHomePageLoadedFirstTime(Context context) {
        if(isHomePageLoadedFirstTime == null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(FFLOK_SHARED_PREFS, Context.MODE_PRIVATE);
            isHomePageLoadedFirstTime = sharedPreferences.getBoolean(HOME_PAGE_FIRST_TIME_LOADED, true);
        }
        return isHomePageLoadedFirstTime;
    }

    public void setHomePageLoadedFirstTime(Boolean homePageLoadedFirstTime, Context context) {
        this.isHomePageLoadedFirstTime = isHomePageLoadedFirstTime;
        SharedPreferences sharedPreferences = context.getSharedPreferences(FFLOK_SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(HOME_PAGE_FIRST_TIME_LOADED, homePageLoadedFirstTime);
        editor.commit();
    }

    public List<Place> getProximatePlaces() {
        return proximatePlaces;
    }

    public void setProximatePlaces(List<Place> proximatePlaces) {
        this.proximatePlaces = proximatePlaces;
    }

    public String getFflokId() {
        return fflokId;
    }

    public void setFflokId(String fflokId) {
        this.fflokId = fflokId;
    }

    public Fflok getFflok() {
        return fflok;
    }

    public void setFflok(Fflok fflok) {
        this.fflok = fflok;
    }
}
