package com.orevon.fflok.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orevon.fflok.R;

public class ProfileExpandableItem extends LinearLayout{
        public TextView titleTextView;
        public TextView subTitleTextView;
        public ImageView imageView ;
        public View divider;

        public ProfileExpandableItem(Context context) {
            super(context);
            initViews(context, null);
        }

        public ProfileExpandableItem(Context context, AttributeSet attrs) {
            super(context, attrs);
            initViews(context, attrs);
        }

        public ProfileExpandableItem(Context context, AttributeSet attrs, int defStyle) {
            this(context, attrs);
            initViews(context, attrs);
        }

        private void initViews(Context context, AttributeSet attrs) {
            LayoutInflater.from(context).inflate(R.layout.profile_expandable_item, this);
            titleTextView = (TextView) this.findViewById(R.id.title);
            subTitleTextView = (TextView) this.findViewById(R.id.subTitle);
            imageView = (ImageView) this.findViewById(R.id.imageView);
            divider = (View) this.findViewById(R.id.divider);
        }
}
