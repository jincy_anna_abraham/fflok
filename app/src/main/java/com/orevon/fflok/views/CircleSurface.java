package com.orevon.fflok.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.SurfaceView;


public class CircleSurface extends SurfaceView {
    private Context mContext;
    private Path clipPath;

    public CircleSurface(Context context, int x, int y, int radius) {
        super(context);
        init(context, x, y, radius);
    }

    public CircleSurface(Context context) {
        super(context);
        init(context, 250, 250, 250);
    }

    public CircleSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, 250, 250, 250);
    }

    public CircleSurface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, 250, 250, 250);
    }

    private void init(Context context, int x, int y, int radius) {
        mContext = context;
        clipPath = new Path();
        clipPath.addCircle(x, y, radius, Path.Direction.CW);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
    }
}
