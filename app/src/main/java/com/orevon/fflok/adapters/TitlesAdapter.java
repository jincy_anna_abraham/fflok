package com.orevon.fflok.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orevon.fflok.R;
import com.orevon.fflok.models.ContactRecord;
import com.orevon.fflok.models.Title;

import java.util.List;

public class TitlesAdapter extends RecyclerView.Adapter<TitlesAdapter.ViewHolder> {

    private List<Title> mItems;
    private final Context mContext;
    int selectedEventPos = 0;
    private TitleClickedCallBack callback;

    public TitlesAdapter(Context context, List<Title> items, int selectionPosition) {
        mItems = items;
        mContext = context;
        selectedEventPos =selectionPosition;
    }

    @Override
    public TitlesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fuss_list, parent, false);
        return new TitlesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TitlesAdapter.ViewHolder holder, final int position) {
        holder.mItem = mItems.get(position);
        holder.name.setText(mItems.get(position).getText());
        holder.name.setTextColor(ContextCompat.getColor(mContext,
                position == selectedEventPos ? R.color.welcomeGreen : R.color.textGray));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback != null) {
                    callback.titleClicked(position, mItems.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final TextView name;

        public Title mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            name = view.findViewById(R.id.name);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "" + "'";
        }
    }

    public void setCallback(TitleClickedCallBack callback){
        this.callback = callback;
    }

    public interface TitleClickedCallBack{
        public void titleClicked(int position, Title title);
    }
}
