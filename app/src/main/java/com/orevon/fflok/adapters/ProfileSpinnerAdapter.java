package com.orevon.fflok.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.orevon.fflok.R;
import com.orevon.fflok.models.ProfileSpinnerModel;

import java.util.List;

public class ProfileSpinnerAdapter extends BaseAdapter{

    Context context;
    List<ProfileSpinnerModel> spinnerModelList;
    LayoutInflater inflator;

    public ProfileSpinnerAdapter(Context context, List<ProfileSpinnerModel> spinnerModels) {
        this.context = context;
        this.spinnerModelList = spinnerModels;
        inflator = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return spinnerModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflator.inflate(R.layout.profile_spinner_item, null);
        ImageView icon = (ImageView) view.findViewById(R.id.image_icon);
        TextView textView = (TextView) view.findViewById(R.id.spinnerText);
        ImageView dropdownIcon = (ImageView) view.findViewById(R.id.dropdown_icon);
        icon.setImageResource(spinnerModelList.get(i).getImageId());
        textView.setText(spinnerModelList.get(i).getTitle());
        dropdownIcon.setVisibility(View.GONE);

        return view;
    }
}
