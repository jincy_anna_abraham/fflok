package com.orevon.fflok.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orevon.fflok.R;
import com.orevon.fflok.models.CheckBoxItem;
import com.orevon.fflok.models.ProfileListItem;
import com.orevon.fflok.views.ProfileExpandableItem;

import java.util.ArrayList;
import java.util.List;

public class ProfileListAdapter extends RecyclerView.Adapter<ProfileListAdapter.MyViewHolder> {

    private List<ProfileListItem> itemList;
    private Context context;
    private OnItemClicked onClick;

    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subTitle, rating, ratingDescription;
        public ImageView thumbImageView;
        public ProgressBar ratingBar;
        public RelativeLayout dropDownLayout;
        public LinearLayout expandableView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.titleTextView);
            subTitle = (TextView) view.findViewById(R.id.subTitleTextView);
            rating = (TextView) view.findViewById(R.id.ratingTextView);
            ratingDescription = (TextView) view.findViewById(R.id.ratingDescriptionTextView);
            thumbImageView = (ImageView) view.findViewById(R.id.image_icon);
            ratingBar = (ProgressBar) view.findViewById(R.id.ratingProgressBar);
            dropDownLayout = (RelativeLayout) view.findViewById(R.id.dropdownLayout);
            expandableView = view.findViewById(R.id.expandableView);
        }
    }


    public ProfileListAdapter(List<ProfileListItem> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_card_rating_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ProfileListItem item = itemList.get(position);
        holder.title.setText(item.getTitle());
        holder.subTitle.setText(item.getSubTitle());
        holder.rating.setText(item.getRating()+"/"+item.getRatingTotal());
        holder.ratingDescription.setText(item.getRatingDescription());
        holder.thumbImageView.setImageResource(item.getImageId());
        int progress = ((item.getRating() * 100 )/item.getRatingTotal());
        holder.ratingBar.setProgress(progress);
//        holder.ratingBar.getIndeterminateDrawable().setColorFilter(item.getColor(), PorterDuff.Mode.MULTIPLY);
        holder.dropDownLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onItemClick(position);
            }
        });

        if (itemList.get(position).isExpanded()) {
            holder.expandableView.setVisibility(View.VISIBLE);
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.whiteSmoke));
            holder.subTitle.setTextColor(ContextCompat.getColor(context, R.color.whiteSmoke));
            holder.ratingDescription.setTextColor(ContextCompat.getColor(context, R.color.whiteSmoke));
            holder.rating.setTextColor(ContextCompat.getColor(context, R.color.whiteSmoke));
            holder.ratingBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.profile_progress));
            holder.dropDownLayout.setBackgroundColor(ContextCompat.getColor(context, item.getColor()));
            holder.expandableView.removeAllViews();
            if (item.getCheckBoxItems() != null) {
                for (CheckBoxItem checkboxItem: item.getCheckBoxItems()) {
                    ProfileExpandableItem itemView = new ProfileExpandableItem(holder.itemView.getContext());
                    itemView.titleTextView.setText(checkboxItem.getTitle());
                    itemView.subTitleTextView.setText(checkboxItem.getSubTitle());
                    if(checkboxItem.isChecked()) {
                        itemView.imageView.setImageResource(R.drawable.red_checkbox_ticked);
                    } else {
                        itemView.imageView.setImageResource(R.drawable.border_circle_white_fill);
                    }
                    if(holder.expandableView.getChildCount() == (item.getCheckBoxItems().size()-1)) {
                        itemView.divider.setVisibility(View.GONE);
                    } else {
                        itemView.divider.setVisibility(View.VISIBLE);
                    }
                    holder.expandableView.addView(itemView);
                }
            }
            holder.expandableView.setBackgroundColor(ContextCompat.getColor(context, item.getColor()));

        } else {
            holder.title.setTextColor(ContextCompat.getColor(context, item.getColor()));
            holder.subTitle.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.ratingDescription.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.rating.setTextColor(ContextCompat.getColor(context, item.getColor()));
            holder.ratingBar.setProgressDrawable(context.getResources().getDrawable(item.getProgressDrawableImageId()));
            holder.dropDownLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.expandableView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }
}

