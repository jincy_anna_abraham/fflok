package com.orevon.fflok.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.orevon.fflok.R;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public MapInfoWindowAdapter(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.map_info_view, null);

        TextView nameTextView = view.findViewById(R.id.name);
        TextView addressTextView = view.findViewById(R.id.address);
        TextView distanceTextView = view.findViewById(R.id.distance);
        final ImageView profileImage = view.findViewById(R.id.imageViewProfilePic);
        SimpleRatingBar ratingBar = view.findViewById(R.id.rating_bar);
        TextView phoneTextView = view.findViewById(R.id.phone);
        final ImageView favImageView = view.findViewById(R.id.fav_image);

        nameTextView.setText(marker.getTitle());

        final Place infoWindowData = (Place) marker.getTag();

//        Glide.with(context).load(infoWindowData.getPathImg()).into(new SimpleTarget<Drawable>() {
//            @Override
//            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    profileImage.setImageDrawable(resource);
//                }
//            }
//        });

        Picasso.get().load(infoWindowData.getPathImg()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                profileImage.setImageBitmap(bitmap);
                profileImage.setScaleX(1);
                profileImage.setScaleY(1);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                profileImage.setImageResource(R.drawable.location1);
                profileImage.setScaleX((float)0.5);
                profileImage.setScaleY((float)0.5);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        nameTextView.setText(infoWindowData.getName());
        addressTextView.setText(infoWindowData.getAdresse());
//        Double distance = CommonUtils.getDistanceBetweenLocations(FflokDataSingleton.getInstance().getLatitude(), FflokDataSingleton.getInstance().getLongitude(), Double.valueOf(infoWindowData.getLatitude()), Double.valueOf(infoWindowData.getLongtitude()));
//        distanceTextView.setText("a " + String.format("%.1f",distance) + " km");
        distanceTextView.setText("a 5 km");
        if(infoWindowData.getPhone() == null) {
            phoneTextView.setText("");
        } else {
            phoneTextView.setText(infoWindowData.getPhone());
        }
        if(infoWindowData.getVote() == null || infoWindowData.getVote().equals("")) {
            ratingBar.setRating(0);
        } else {
            ratingBar.setRating(Float.valueOf(infoWindowData.getVote()));
        }
        if(infoWindowData.getSelected()) {
//            favImageView.getBackground().setColorFilter(context.getResources().getColor(R.color.white),
//                    PorterDuff.Mode.SRC_ATOP);
            favImageView.getBackground().setColorFilter(context.getResources().getColor(R.color.nouvelle_green),
                    PorterDuff.Mode.SRC_ATOP);
        } else {
            favImageView.getBackground().setColorFilter(context.getResources().getColor(R.color.white),
                    PorterDuff.Mode.SRC_ATOP);
        }
        return view;
    }

}

