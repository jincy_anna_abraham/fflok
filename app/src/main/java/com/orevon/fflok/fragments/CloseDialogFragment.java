package com.orevon.fflok.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.orevon.fflok.R;

public class CloseDialogFragment extends DialogFragment {

    private View closeFragment;
    private RelativeLayout yesbutton;
    private RelativeLayout nobutton;

    public CloseDialogFragment() {

    }


    public static CloseDialogFragment newInstance() {
        CloseDialogFragment fragment = new CloseDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.MessageDialogTheme);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.MessageDialogTheme;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return dialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        closeFragment = inflater.inflate(R.layout.dialog_close_layout, container, false);
        yesbutton = closeFragment.findViewById(R.id.yesbutton);
        nobutton = closeFragment.findViewById(R.id.nobutton);
//        OkButton = shiftFragment.findViewById(R.id.OkButton);
//

//        OkButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (cur_pass.getText().toString().isEmpty() || new_pas.getText().toString().isEmpty()) {
//                    Snackbar.make(v, "Please fill all fields", Snackbar.LENGTH_SHORT).show();
//                    return;
//                }
//
//                if (new_pas.getText().toString().length() < 6) {
//                    Snackbar.make(v, "Password is not strong", Snackbar.LENGTH_SHORT).show();
//                    return;
//                }
//                changepasswordApi(cur_pass.getText().toString(), new_pas.getText().toString(), v);
//
//            }
//        });

        yesbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        nobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return closeFragment;
    }


}
