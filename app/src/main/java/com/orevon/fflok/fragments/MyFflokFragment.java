package com.orevon.fflok.fragments;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.headerfooter.songhang.library.SmartRecyclerAdapter;
import com.orevon.fflok.R;
import com.orevon.fflok.utils.AndroidUtils;
import com.orevon.fflok.utils.SpacesItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFflokFragment extends Fragment {


    private View headerView;
    private RecyclerView recyclerView;
    private RecyclerView progressRecyclerView;
    private LinearLayout collapseContainer;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private View intro;

    private int commenceClickCount = 0;

    public MyFflokFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_fflok, container, false);
        progressRecyclerView = view.findViewById(R.id.progessList);
        recyclerView = view.findViewById(R.id.list);
        headerView = inflater.inflate(R.layout.ffloks_precedents_header, recyclerView, false);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        collapseContainer = view.findViewById(R.id.collapseContainer);
        collapsingToolbarLayout = view.findViewById(R.id.collapsingToolbarLayout);
        appBarLayout = view.findViewById(R.id.appBarLayout);
        intro = view.findViewById(R.id.intro);
//        setupCollapse();
        initPrecedentsRecyclerView();
        initProgressRecyclerView();
        setUpIntro();
        setupCommenceButton();
    }

    private void setUpIntro() {
        // 68 is the height of the BottomNavigationView
        appBarLayout.setLayoutParams(new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT,
                AndroidUtils.getScreenHeight(getActivity()) - AndroidUtils.dpToPx(getContext(), 68) - 24));

        AppBarLayout.LayoutParams p = (AppBarLayout.LayoutParams) collapsingToolbarLayout.getLayoutParams();
        p.setScrollFlags(0);
        collapsingToolbarLayout.setLayoutParams(p);
    }

    private void setupCommenceButton() {
        Button button = intro.findViewById(R.id.commence);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commenceClickCount++;
                if (commenceClickCount == 1) {
                    appBarLayout.setLayoutParams(new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT,
                            CoordinatorLayout.LayoutParams.WRAP_CONTENT));

                    AppBarLayout.LayoutParams p = (AppBarLayout.LayoutParams) collapsingToolbarLayout.getLayoutParams();
                    p.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                            | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                    collapsingToolbarLayout.setLayoutParams(p);
                    recyclerView.setVisibility(View.VISIBLE);
                    setupCollapse();
                } else {
                    intro.setVisibility(View.GONE);
                    collapseContainer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setupCollapse() {
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(getContext(), R.color.welcomeGreen));

        Typeface titleFont = Typeface.
                createFromAsset(getContext().getAssets(), "fonts/CircularStd-Bold.otf");
        collapsingToolbarLayout.setCollapsedTitleTypeface(titleFont);
        collapsingToolbarLayout.setExpandedTitleTypeface(titleFont);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset < 150) {
                    collapsingToolbarLayout.setTitle(getString(R.string.ffloks_in_progress));
//                    toolbar.setTitle(getString(R.string.ffloks_in_progress));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

    private void initPrecedentsRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,
//                AndroidUtils.dpToPx(getContext(), 16)));
//        recyclerView.addItemDecoration(new SpacesItemDecoration(
//                AndroidUtils.dpToPx(getContext(), 16)));

        SmartRecyclerAdapter smartRecyclerAdapter = new SmartRecyclerAdapter(
                new PrecedentsAdapter(getContext()));
        smartRecyclerAdapter.setHeaderView(headerView);
        recyclerView.setAdapter(smartRecyclerAdapter);
    }

    private void initProgressRecyclerView() {
        progressRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,
//                AndroidUtils.dpToPx(getContext(), 16)));
        recyclerView.addItemDecoration(new SpacesItemDecoration(
                AndroidUtils.dpToPx(getContext(), 16)));

//        SmartRecyclerAdapter smartRecyclerAdapter = new SmartRecyclerAdapter(
//                new ProgressAdapter(getContext()));
        progressRecyclerView.setAdapter(new ProgressAdapter(getContext()));
    }

    private class PrecedentsAdapter extends RecyclerView.Adapter<PrecedentsAdapter.ViewHolder> {

        //        private final List<Item> mItems;
        private final Context mContext;

        PrecedentsAdapter(Context context) {
//            mItems = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.ffloks_precedents, parent, false);
            return new PrecedentsAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
//            holder.mItem = mItems.get(position);
//            holder.title.setText(holder.mItem.getName());
//            Picasso.with(mContext).load(holder.mItem.getImage()).into(holder.image);
        }

        @Override
        public int getItemCount() {
//            return mItems.size();
            return 20;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            final ImageView image;
            final TextView title;
//            public Item mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                image = view.findViewById(R.id.image);
                title = view.findViewById(R.id.title);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + "" + "'";
            }
        }
    }

    private class ProgressAdapter extends RecyclerView.Adapter<ProgressAdapter.ViewHolder> {

        //        private final List<Item> mItems;
        private final Context mContext;

        ProgressAdapter(Context context) {
//            mItems = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.ffloks_in_progress, parent, false);
            return new ProgressAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
//            holder.mItem = mItems.get(position);
//            holder.title.setText(holder.mItem.getName());
//            Picasso.with(mContext).load(holder.mItem.getImage()).into(holder.image);
            holder.peoplesList.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
            holder.peoplesList.setAdapter(new PeoplesAdapter(getContext()));
        }

        @Override
        public int getItemCount() {
//            return mItems.size();
            return 8;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            final ImageView image;
            final TextView title;
            final RecyclerView peoplesList;
//            public Item mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                image = view.findViewById(R.id.image);
                title = view.findViewById(R.id.title);
                peoplesList = view.findViewById(R.id.peoplesList);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + "" + "'";
            }
        }
    }

    private class PeoplesAdapter extends RecyclerView.Adapter<PeoplesAdapter.ViewHolder> {

        //        private final List<Item> mItems;
        private final Context mContext;

        PeoplesAdapter(Context context) {
//            mItems = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fflok_progress_people, parent, false);
            return new PeoplesAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
//            holder.mItem = mItems.get(position);
//            holder.title.setText(holder.mItem.getName());
//            Picasso.with(mContext).load(holder.mItem.getImage()).into(holder.image);

        }

        @Override
        public int getItemCount() {
//            return mItems.size();
            return 3;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
//            public Item mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
            }

            @Override
            public String toString() {
                return super.toString() + " '" + "" + "'";
            }
        }
    }

}
