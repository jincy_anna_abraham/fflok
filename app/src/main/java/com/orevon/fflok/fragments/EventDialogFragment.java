package com.orevon.fflok.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.orevon.fflok.R;

public class EventDialogFragment extends DialogFragment {

    private View eventFragment;
    private LinearLayout democrate;
    private LinearLayout tyrant;
    private ImageView closebutton;
    private int selcetion_stat = 0; //0- nothing selected, 1- democrate , 2- tyrant
    private RelativeLayout demo_sel, tyrant_sel, confirmbutton;

    public EventDialogFragment() {

    }


    public static EventDialogFragment newInstance() {
        EventDialogFragment fragment = new EventDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.MessageDialogTheme);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.MessageDialogTheme;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return dialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        eventFragment = inflater.inflate(R.layout.dialog_event_layout, container, false);
//        democrate = eventFragment.findViewById(R.id.democrate);
//        tyrant = eventFragment.findViewById(R.id.tyrant);
        closebutton = eventFragment.findViewById(R.id.closebutton);
//        demo_sel = eventFragment.findViewById(R.id.demo_sel);
//        tyrant_sel = eventFragment.findViewById(R.id.tyrant_sel);
//        confirmbutton = eventFragment.findViewById(R.id.confirmbutton);

//        democrate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selcetion_stat = 1;
//                demo_sel.setVisibility(View.VISIBLE);
//                tyrant_sel.setVisibility(View.GONE);
//
//            }
//        });
//
//        tyrant.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selcetion_stat = 2;
//                demo_sel.setVisibility(View.GONE);
//                tyrant_sel.setVisibility(View.VISIBLE);
//            }
//        });
//
//        confirmbutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (selcetion_stat == 1) {
//
//                    DemocratDialogFragment fragment = DemocratDialogFragment.newInstance();
//                    fragment.show(getActivity().getFragmentManager(), "");
//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            // Do something after 5s = 5000ms
//                            dismiss();
//                        }
//                    }, 2000);
//
//                } else if (selcetion_stat == 2) {
//                    TyrantDialogFragment fragment = TyrantDialogFragment.newInstance();
//                    fragment.show(getActivity().getFragmentManager(), "");
//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            // Do something after 5s = 5000ms
//                            dismiss();
//                        }
//                    }, 2000);
//                }
//            }
//        });

        closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return eventFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ConstraintLayout layout = view.findViewById(R.id.item2);
        layout.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.group2));
    }


}
