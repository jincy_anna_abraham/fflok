package com.orevon.fflok.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.orevon.fflok.R;

public class EventMapFragment extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    BottomSheetBehavior bottomSheetBehavior;
    boolean istemplateclicked = false;
    boolean isgiftemplateclicked = false;
    RelativeLayout giftemplatelay;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CardView chat_bottom_sheet;
    private RelativeLayout topimageLay, bottomimageLay;
    private View toptint, bottomtint;
    private ImageView closeButton, texttemplate;
    private RelativeLayout toplay;
    private ScrollView texttemplatelay;
    private ImageView giftemplate;
    private GoogleMap mMap;


    public EventMapFragment() {
        // Required empty public constructor
    }

//    public static MyFflok2Fragment newInstance(String param1, String param2) {
//        MyFflok2Fragment fragment = new MyFflok2Fragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.eventmap_fragment, container, false);
//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(EventMapFragment.this);

        FragmentManager fm = getActivity().getSupportFragmentManager();/// getChildFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, supportMapFragment).commit();
        }
        supportMapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setupBottomSheet();
        clickListeners(view);


    }

    private void clickListeners(View view) {

        toplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseDialogFragment fragment = CloseDialogFragment.newInstance();
                fragment.show(getActivity().getFragmentManager(), "");
            }
        });


        texttemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                giftemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                giftemplatelay.setVisibility(View.GONE);
                isgiftemplateclicked = false;
                if (istemplateclicked) {
                    texttemplatelay.setVisibility(View.GONE);
                    istemplateclicked = false;
                    texttemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                } else {
                    texttemplatelay.setVisibility(View.VISIBLE);
                    istemplateclicked = true;
                    texttemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.welcomeGreen));
                }

            }
        });

        giftemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                texttemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                texttemplatelay.setVisibility(View.GONE);
                istemplateclicked = false;
                if (isgiftemplateclicked) {
                    giftemplatelay.setVisibility(View.GONE);
                    isgiftemplateclicked = false;
                    giftemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                } else {
                    giftemplatelay.setVisibility(View.VISIBLE);
                    isgiftemplateclicked = true;
                    giftemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.welcomeGreen));
                }

            }
        });
    }

    private void setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(chat_bottom_sheet);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                } else if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void initView(View view) {
        chat_bottom_sheet = view.findViewById(R.id.chat_bottom_sheet);
        closeButton = view.findViewById(R.id.closebutton);
        toplay = view.findViewById(R.id.toplay);
        texttemplate = view.findViewById(R.id.texttemplate);
        texttemplatelay = view.findViewById(R.id.texttemplatelay);
        giftemplatelay = view.findViewById(R.id.giftemplatelay);
        giftemplate = view.findViewById(R.id.giftemplate);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}

