package com.orevon.fflok.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;

import com.orevon.fflok.R;
import com.orevon.fflok.adapters.ConfidentialiteListAdapter;
import com.orevon.fflok.enums.ConfidentialiteItemType;
import com.orevon.fflok.models.ConfidentialiteItem;
import com.orevon.fflok.models.DataPartner;
import com.orevon.fflok.models.DataShared;
import com.orevon.fflok.models.SwitchItem;

import java.util.ArrayList;
import java.util.List;

public class ConfidentialiteFragment extends Fragment implements ConfidentialiteListAdapter.OnItemClicked{

    private RecyclerView recyclerView;
    private List<ConfidentialiteItem> data = new ArrayList<>();
    private ConfidentialiteListAdapter adapter;
    private Button alertButton;
    private ImageButton backButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_confidentialite, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        backButton = (ImageButton) view.findViewById(R.id.backButton);
        alertButton = (Button) view.findViewById(R.id.alertButton);
        setupViews();
        setData();
        return view;
    }

    private void setupViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        //fetch data and on ExpandableRecyclerAdapter
        adapter = new ConfidentialiteListAdapter(data, getContext());
        adapter.setOnClick(this);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                fm.popBackStack ("ProfileFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);

            }
        });
        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.confidentialite_alert, null);
                final View bgView = dialogView.findViewById(R.id.alert_bg_view);
                bgView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        bgView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        android.view.ViewGroup.LayoutParams mParams = bgView.getLayoutParams();
                        mParams.height = bgView.getWidth();
                        bgView.setLayoutParams(mParams);
                    }
                });
                dialogBuilder.setView(dialogView);

                Button yesButton = (Button) dialogView.findViewById(R.id.yesButton);
                Button noButton = (Button) dialogView.findViewById(R.id.noButton);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
//                alertDialog.getWindow().setLayout(500,600);
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertDialog.getWindow().setDimAmount((float)0.1);

                yesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

            }
        });

    }

    private void setData() {

        ConfidentialiteItem item = new ConfidentialiteItem("Ton attitude", ConfidentialiteItemType.TimeLine, true);
        data.add(item);

        item = new ConfidentialiteItem("Tes donnees", ConfidentialiteItemType.CheckBoxList, false);
        List<SwitchItem> switchItems = new ArrayList<>();
        switchItems.add(new SwitchItem("Photo profil", false));
        switchItems.add(new SwitchItem("Nom prenom", false));
        switchItems.add(new SwitchItem("Address postale", true));
        switchItems.add(new SwitchItem("Ages", false));
        switchItems.add(new SwitchItem("Telephone", true));
        switchItems.add(new SwitchItem("E-mail", true));
        item.setSwitchItems(switchItems);
        data.add(item);

        item = new ConfidentialiteItem("Les sources", ConfidentialiteItemType.CheckBoxList, false);
        switchItems = new ArrayList<>();
        switchItems.add(new SwitchItem("facebook", true, R.drawable.facebook));
        switchItems.add(new SwitchItem("twitter", false, R.drawable.twitter));
        switchItems.add(new SwitchItem("slack", false, R.drawable.slack));
        switchItems.add(new SwitchItem("google +", false, R.drawable.googleplus));
        switchItems.add(new SwitchItem("linkedin", true, R.drawable.linkedin));
        switchItems.add(new SwitchItem("snapchat", false, R.drawable.snapchat));
        switchItems.add(new SwitchItem("instagram", true, R.drawable.insta));
        item.setSwitchItems(switchItems);
        data.add(item);

        item = new ConfidentialiteItem("Ce qu'on fait avec", ConfidentialiteItemType.CheckBoxList, false);
        switchItems = new ArrayList<>();
        switchItems.add(new SwitchItem("Mes amis du nid", false));
        switchItems.add(new SwitchItem("Proposition restaurant", false));
        switchItems.add(new SwitchItem("Piste peloton en rouge", false));
        switchItems.add(new SwitchItem("Generation notification", false));
        item.setSwitchItems(switchItems);
        data.add(item);

        item = new ConfidentialiteItem("Avec qui on les partage", ConfidentialiteItemType.PartnersList, false);
        List<DataPartner> dataPartners = new ArrayList<>();
        List<DataShared> dataSharedList = new ArrayList<>();
        dataSharedList.add(new DataShared("Jamais", R.color.pulse, true));
        dataSharedList.add(new DataShared("Ovi", R.color.nouvelle_green, false));
        dataSharedList.add(new DataShared("Consentent", R.color.blue,false));
        dataSharedList.add(new DataShared("Anonyme", R.color.nouvelle_green,false));
        dataSharedList.add(new DataShared("Agreger", R.color.blue,false));
        dataPartners.add(new DataPartner(R.drawable.partner1, dataSharedList));

        dataSharedList = new ArrayList<>();
        dataSharedList.add(new DataShared("Jamais", R.color.pulse, false));
        dataSharedList.add(new DataShared("Ovi", R.color.nouvelle_green, true));
        dataSharedList.add(new DataShared("Consentent", R.color.blue,false));
        dataSharedList.add(new DataShared("Anonyme", R.color.nouvelle_green,false));
        dataSharedList.add(new DataShared("Agreger", R.color.blue,false));
        dataPartners.add(new DataPartner(R.drawable.partner2, dataSharedList));

        dataSharedList = new ArrayList<>();
        dataSharedList.add(new DataShared("Jamais", R.color.pulse, false));
        dataSharedList.add(new DataShared("Ovi", R.color.nouvelle_green, true));
        dataSharedList.add(new DataShared("Consentent", R.color.blue,true));
        dataSharedList.add(new DataShared("Anonyme", R.color.nouvelle_green,false));
        dataSharedList.add(new DataShared("Agreger", R.color.blue,true));
        dataPartners.add(new DataPartner(R.drawable.partner3, dataSharedList));

        item.setDataPartners(dataPartners);
        data.add(item);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        boolean isExpanded = !data.get(position).isExpanded();
        if (isExpanded) {
            for (ConfidentialiteItem item:
                    data) {

                item.setExpanded(false);
            }
        }
        data.get(position).setExpanded(isExpanded);
        data.get(0).setExpanded(true);
        adapter.notifyDataSetChanged();
    }
}
