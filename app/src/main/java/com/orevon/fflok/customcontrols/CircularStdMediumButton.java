package com.orevon.fflok.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CircularStdMediumButton extends android.support.v7.widget.AppCompatButton {

    public CircularStdMediumButton(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CircularStdMediumButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CircularStdMediumButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Medium.otf");
        setTypeface(tf);
    }
}


