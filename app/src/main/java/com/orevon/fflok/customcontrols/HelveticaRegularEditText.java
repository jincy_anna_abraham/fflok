package com.orevon.fflok.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class HelveticaRegularEditText extends AppCompatEditText {

    public HelveticaRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public HelveticaRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HelveticaRegularEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Helvetica-Regular.ttf");
            setTypeface(tf);
        }
    }

}
