package com.orevon.fflok.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CircularStdBoldTextView extends android.support.v7.widget.AppCompatTextView {

    public CircularStdBoldTextView(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CircularStdBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CircularStdBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Bold.otf");
        setTypeface(tf);
    }
}
