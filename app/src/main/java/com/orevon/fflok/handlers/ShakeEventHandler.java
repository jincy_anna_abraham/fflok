package com.orevon.fflok.handlers;

/**
 * Handle Shake (Accelerometer) events
 */
public interface ShakeEventHandler {
    void handleShakeEvent();
}
